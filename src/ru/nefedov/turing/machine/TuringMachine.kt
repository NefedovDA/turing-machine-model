package ru.nefedov.turing.machine

class TuringMachine(
    private val start: State,
    private val accept: State,
    private val reject: State,
    private val blankSymbol: Char
) {
    enum class TuringMachineResult { Accept, Reject }

    fun run(input: String, assemblyLineCreator: (Char, String) -> AssemblyLine = ::AssemblyLine): TuringMachineResult {
        val assemblyLine = assemblyLineCreator(blankSymbol, input)
        var currentSymbol = assemblyLine.getCurrentSymbol()
        var currentState = start

        while (true) {
            when (currentState) {
                accept -> return TuringMachineResult.Accept
                reject -> return TuringMachineResult.Reject
                else -> {
                    val (direction, setSymbol, toState) = currentState[currentSymbol]
                        ?: return TuringMachineResult.Reject

                    currentSymbol = assemblyLine.step(direction, setSymbol)
                    currentState = toState
                }
            }
        }
    }
}

fun main() {
    val start = State("s")
    val medium = State("m")
    val accept = State("ac")
    val reject = State("rj")

    start['_'] = State.StepInfo(Direction.Stay, '_', accept)
    start['0'] = State.StepInfo(Direction.Forward, '_', medium)
    medium['0'] = State.StepInfo(Direction.Forward, '_', start)
    medium['_'] = State.StepInfo(Direction.Stay, '_', reject)

    val turingMachine = TuringMachine(start, accept, reject, '_')

    println(
        turingMachine.run("")
    )
    println(
        turingMachine.run("00")
    )
    println(
        turingMachine.run("000")
    )
}