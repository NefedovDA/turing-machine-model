package ru.nefedov.turing.machine

class PrintableAssemblyLine(blankSymbol: Char, stringLine: String) : AssemblyLine(blankSymbol, stringLine) {
    override fun step(direction: Direction, putSymbol: Char): Char {
        val returnSymbol = super.step(direction, putSymbol)
        print()
        return returnSymbol
    }

    private fun print() {
        print(blankSymbol)
        line.forEach { print(it) }
        println(blankSymbol)
        println(" ".repeat(position + 1) + "^")
    }
}