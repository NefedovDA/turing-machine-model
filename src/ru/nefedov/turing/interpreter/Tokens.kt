package ru.nefedov.turing.interpreter

import ru.nefedov.turing.machine.Direction

sealed class Line

sealed class PreprocessorValue<TypeT : Enum<TypeT>>(val name: String, val value: String) : Line() {
    abstract val type: TypeT
}

data class PreprocessorState(override val type: Type, val stateName: String) :
    PreprocessorValue<PreprocessorState.Type>(type.name.toLowerCase(), stateName) {
    enum class Type {
        START,
        ACCEPT,
        REJECT
    }
}

data class PreprocessorSymbol(override val type: Type, val symbol: Char) :
    PreprocessorValue<PreprocessorSymbol.Type>(type.name.toLowerCase(), "$symbol") {
    enum class Type {
        BLANK
    }
}

data class ShiftLine(
    val fromStateName: String,
    val atSymbol: Char,
    val toStateName: String,
    val putSymbol: Char,
    val direction: Direction
) : Line()