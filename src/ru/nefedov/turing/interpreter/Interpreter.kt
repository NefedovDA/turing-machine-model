package ru.nefedov.turing.interpreter

import ru.nefedov.turing.interpreter.PreprocessorState.Type.*
import ru.nefedov.turing.interpreter.PreprocessorSymbol.Type.BLANK
import ru.nefedov.turing.machine.PrintableAssemblyLine
import ru.nefedov.turing.machine.State
import ru.nefedov.turing.machine.TuringMachine
import java.io.File
import java.util.function.Function

class Interpreter : Function<File, TuringMachine> {
    override fun apply(file: File): TuringMachine {
        val lines = Tokenizer().process(file)
        val preprocessorLines = lines.filterIsInstance<PreprocessorValue<*>>()
        val shiftLines = lines.filterIsInstance<ShiftLine>()

        val stateMap = mutableMapOf<String, State>()


        fun <T> findOrDefault(type: T, default: String): String =
            preprocessorLines.find { it.type == type }?.value ?: default

        fun getOrPutState(name: String): State = stateMap.getOrPut(name) { State(name) }


        val start = getOrPutState(findOrDefault(START, START_DEFAULT))
        val accept = getOrPutState(findOrDefault(ACCEPT, ACCEPT_DEFAULT))
        val reject = getOrPutState(findOrDefault(REJECT, REJECT_DEFAULT))

        val blankSymbol = findOrDefault(BLANK, "$BLANK_DEFAULT")[0]

        shiftLines.forEach {
            with(it) {
                val fromState = getOrPutState(fromStateName)
                val toState = getOrPutState(toStateName)
                fromState[atSymbol] = State.StepInfo(direction, putSymbol, toState)
            }
        }

        return TuringMachine(start, accept, reject, blankSymbol)
    }

    companion object {
        private const val START_DEFAULT = "s"
        private const val ACCEPT_DEFAULT = "ac"
        private const val REJECT_DEFAULT = "rj"
        private const val BLANK_DEFAULT = '_'
    }
}

fun main(args: Array<String>) {
    val filePath = args[0]
    val input = args.getOrNull(1)

    val machine = Interpreter().apply(File(filePath))
    if (input != null) {
        machine.run(input, ::PrintableAssemblyLine)
        return
    }

    while (true) {
        println("Enter input line:")
        val line = readLine() ?: return
        println(machine.run(line, ::PrintableAssemblyLine))
    }
}