package ru.nefedov.turing.machine

import ru.nefedov.turing.machine.Direction.*
import java.util.*

open class AssemblyLine(protected val blankSymbol: Char, stringLine: String = "") {
    protected var position: Int = 0
    protected val line = LinkedList<Char>()

    init {
        if (stringLine.isEmpty()) {
            line += blankSymbol
        }
        stringLine.forEach { line += it }
    }

    fun getCurrentSymbol(): Char = line[position]

    open fun step(direction: Direction, putSymbol: Char): Char {
        line[position] = putSymbol

        when (direction) {
            Forward -> {
                ++position
                if (position == line.size) {
                    line.addLast(blankSymbol)
                }
            }
            Back -> {
                if (position == 0) {
                    line.addFirst(blankSymbol)
                } else {
                    --position
                }
            }
            Stay -> {
            }
        }

        return line[position]
    }
}