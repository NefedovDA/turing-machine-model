package ru.nefedov.turing.machine

class State(val name: String) {
    private val mapper: MutableMap<Char, StepInfo> = HashMap()

    operator fun get(symbol: Char): StepInfo? = mapper[symbol]

    operator fun set(symbol: Char, stepInfo: StepInfo) {
        mapper[symbol] = stepInfo
    }

    data class StepInfo(val direction: Direction, val putSymbol: Char, val toState: State)
}