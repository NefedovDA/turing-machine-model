package ru.nefedov.turing.interpreter

import ru.nefedov.turing.machine.Direction
import java.io.File
import java.text.ParseException

class Tokenizer {
    fun process(file: File): List<Line> =
        file.readLines().mapIndexedNotNull { index, line ->
            if (line.isBlank()) {
                return@mapIndexedNotNull null
            }

            ifMatch(PREPROCESSOR_LINE, line) {
                return@mapIndexedNotNull constructPreprocessorLine(it)
                    ?: throw ParseException("Illegal line: $line", index)
            }

            ifMatch(SHIFT_LINE, line) {
                return@mapIndexedNotNull constructShiftLine(it)
            }

            throw ParseException("Illegal line: $line", index)
        }

    private fun constructPreprocessorLine(args: List<String>): Line? {
        val (name, value) = args

        ifPartOf<PreprocessorState.Type>(name) {
            return PreprocessorState(it, value)
        }

        if (value.length != 1) {
            return null
        }
        val charValue = value[0]

        ifPartOf<PreprocessorSymbol.Type>(name) {
            return PreprocessorSymbol(it, charValue)
        }

        return null
    }

    private fun constructShiftLine(args: List<String>): Line {
        val (fromStateName, atSymbol, toStateName, putSymbol, sDirection) = args
        return ShiftLine(fromStateName, atSymbol[0], toStateName, putSymbol[0], Direction.fromSymbol(sDirection))
    }

    private inline fun <reified T : Enum<T>> ifPartOf(name: String, createAndReturn: (type: T) -> Unit) {
        enumValues<T>().forEach {
            if (it.name.toLowerCase() == name) {
                createAndReturn(it)
            }
        }
    }

    private inline fun ifMatch(regex: Regex, line: String, createAndReturn: (List<String>) -> Unit) {
        val matchResult = regex.find(line)
        if (matchResult != null) {
            createAndReturn(matchResult.destructured.toList())
        }
    }

    companion object {
        private val PREPROCESSOR_LINE = """^\s*(\w+):\s+(\w+)\s*$""".toRegex()
        private val SHIFT_LINE = """^\s*(\w+)\s+(\w)\s+->\s+(\w+)\s+(\w)\s+([<^>])\s*$""".toRegex()
    }
}