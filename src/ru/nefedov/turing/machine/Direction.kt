package ru.nefedov.turing.machine

import java.lang.IllegalArgumentException

enum class Direction {
    Forward, Stay, Back;

    companion object {
        fun fromSymbol(symbol: String): Direction = when (symbol) {
            "<" -> Back
            "^" -> Stay
            ">" -> Forward
            else -> throw IllegalArgumentException()
        }
    }
}